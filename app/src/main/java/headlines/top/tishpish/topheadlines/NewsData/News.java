package headlines.top.tishpish.topheadlines.NewsData;

/**
 * Created by tishpish on 7/15/17.
 */

public class News 
{
    String paperName,  category,   title,   link,   date,   time;
    int views, id;
    public News(String paperName,String category, String title, String link, String date, String time, int views, int id)
    {
        this.paperName = paperName;
        this.category= category;
        this.title = title;
        this.link = link;
        this.date = date;
        this.time  = time;
        this.views = views;
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public String getDate() {
        return date;
    }

    public String getLink() {
        return link;
    }

    public String getPaperName() {
        return paperName;
    }

    public String getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
