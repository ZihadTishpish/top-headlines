package headlines.top.tishpish.topheadlines.NewsData;

/**
 * Created by tishpish on 7/15/17.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import headlines.top.tishpish.topheadlines.R;

public class NewsAdapter extends BaseAdapter{
    ArrayList<News> result;
    Context context;
    //int [] imageId;
    private static LayoutInflater inflater=null;
    public NewsAdapter(Context mainActivity, ArrayList<News> prgmNameList ) {
        // TODO Auto-generated constructor stub
        result=prgmNameList;
        context=mainActivity;
       // imageId=prgmImages;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView title,paper_name,date_time,views,catagory;
        ImageView img;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.news_slot_adapter_design, null);
        holder.title=(TextView) rowView.findViewById(R.id.title);
        holder.catagory=(TextView) rowView.findViewById(R.id.catagory);
        holder.date_time=(TextView) rowView.findViewById(R.id.date_time);
        //holder.views=(TextView) rowView.findViewById(R.id.views);
        holder.paper_name=(TextView) rowView.findViewById(R.id.paper_name);
        holder.img=(ImageView) rowView.findViewById(R.id.image);

        String p_name = result.get(position).getPaperName().trim();
        if (p_name.matches("যুগান্তর"))
            holder.img.setImageResource(R.mipmap.jugantar);
        else if (p_name.matches("বাংলানিউজ২৪"))
            holder.img.setImageResource(R.mipmap.bangla);
        else if (p_name.matches("বাংলা ট্রিবিউন"))
            holder.img.setImageResource(R.mipmap.btribune);
        else if (p_name.matches("কালের কণ্ঠ"))
            holder.img.setImageResource(R.mipmap.kaler);


        holder.title.setText(result.get(position).getTitle());
        holder.catagory.setText(result.get(position).getCategory());
        holder.date_time.setText(result.get(position).getTime()+", "+result.get(position).getDate());
        holder.paper_name.setText(result.get(position).getPaperName());

        //holder.views.setText(result.get(position).getViews()+" views");

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
              //  Toast.makeText(context, "You Clicked "+result.get(position).getLink(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, NewsDetails.class);
                intent.putExtra("LINK", result.get(position).getLink());
                intent.putExtra("ID",result.get(position).getId()+"");
                context.startActivity(intent);
            }
        });
        return rowView;
    }

} 