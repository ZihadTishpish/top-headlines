package headlines.top.tishpish.topheadlines;

import android.content.IntentFilter;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import headlines.top.tishpish.topheadlines.Category.CategoryBasedView;
import headlines.top.tishpish.topheadlines.Category.Trending;
import headlines.top.tishpish.topheadlines.NewsData.AppSharedPreference;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        displaySelectedScreen(R.id.nav_home);

        /*
        */


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        displaySelectedScreen(item.getItemId());
        return true;
    }

    private void displaySelectedScreen(int itemId)
    {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId)
        {
            case R.id.nav_home:
                //fragment = new DefaultScreen();
                AppSharedPreference aspx = AppSharedPreference.getInstance(getApplicationContext());
                aspx.putSelectedCategory("all");
                fragment = new CategoryBasedView();
                break;
            case R.id.nav_national:
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                asp.putSelectedCategory("national");
                fragment = new CategoryBasedView();
                break;
            case R.id.nav_sports:
                AppSharedPreference asp2 = AppSharedPreference.getInstance(getApplicationContext());
                asp2.putSelectedCategory("sports");
                fragment = new CategoryBasedView();
                break;
            case R.id.nav_international:
                AppSharedPreference asp3 = AppSharedPreference.getInstance(getApplicationContext());
                asp3.putSelectedCategory("international");
                fragment = new CategoryBasedView();
                break;
            case R.id.nav_entertainment:
                AppSharedPreference asp4 = AppSharedPreference.getInstance(getApplicationContext());
                asp4.putSelectedCategory("entertainment");
                fragment = new CategoryBasedView();
                break;

        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }








}
