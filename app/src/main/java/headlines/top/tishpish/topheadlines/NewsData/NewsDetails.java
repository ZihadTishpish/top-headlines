package headlines.top.tishpish.topheadlines.NewsData;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Debug;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import headlines.top.tishpish.topheadlines.R;
import io.github.yuweiguocn.lib.squareloading.SquareLoading;

public class NewsDetails extends AppCompatActivity {

    WebView webView;
    TextView progress;
    SquareLoading progressIcon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String newString;
        String Id="0";
        if (savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();
            if(extras == null)
            {
                newString= null;
            }
            else
            {
                newString= extras.getString("LINK");
                Id= extras.getString("ID");
            }
        }
        else
        {
            newString= (String) savedInstanceState.getSerializable("LINK");
            Id= (String) savedInstanceState.getSerializable("ID");
        }


        webView = (WebView) findViewById(R.id.webView1);

        progress = (TextView) findViewById(R.id.progress_text);
        progressIcon = (SquareLoading) findViewById(R.id.progress_icon);

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // Handle the error
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                progress.setVisibility(View.GONE);
                progressIcon.setVisibility(View.GONE);
            }



            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });


        webView.setWebChromeClient(new WebChromeClient()
        {

            public void onProgressChanged(WebView view, int newProgress)
            {


                if(newProgress >=90)
                {
                    progress.setVisibility(View.GONE);
                    progressIcon.setVisibility(View.GONE);
                }
            }
        });
        webView.loadUrl(newString);

        updateViewCount(Id);


    }

    public void updateViewCount(final String ID)
    {
        //Toast.makeText(getApplicationContext(),"Update process started",Toast.LENGTH_LONG).show();
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest sr = new StringRequest(Request.Method.POST,links.RequestLink, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                /*if (response != null && response.length() > 0)
                {

                    Toast.makeText(getApplicationContext(),"Updated"+response+" requested: "+ID,Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Failed :/",Toast.LENGTH_LONG).show();
                }*/

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //swipeContainer.setRefreshing(false);
                //Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                //Log.e("tishpish",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("query","UPDATE_VIEW");
                params.put("id",ID+"");
                return params;
            }
        };
        queue.add(sr);
    }

}
