package headlines.top.tishpish.topheadlines;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import headlines.top.tishpish.topheadlines.NewsData.AppSharedPreference;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Splash extends AppCompatActivity
{

    int splashDelay = 800;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {

                    startActivity(new Intent(Splash.this, Home.class));
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();


            }
        }, splashDelay);


    }

    public boolean isLoggedIn()
    {
        AppSharedPreference asp =  AppSharedPreference.getInstance(getApplicationContext());
        if (asp.getLoginInfo().length()>0)
            return true;
        else
            return false;

    }



}
