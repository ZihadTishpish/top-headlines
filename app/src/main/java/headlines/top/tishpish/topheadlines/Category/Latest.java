package headlines.top.tishpish.topheadlines.Category;

/**
 * Created by tishpish on 7/15/17.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import headlines.top.tishpish.topheadlines.NewsData.News;
import headlines.top.tishpish.topheadlines.NewsData.NewsAdapter;
import headlines.top.tishpish.topheadlines.NewsData.links;
import headlines.top.tishpish.topheadlines.R;
import io.github.yuweiguocn.lib.squareloading.SquareLoading;


public class Latest extends Fragment{

    TextView networkBanner;
    ListView lv;
    SquareLoading loading;
    SwipeRefreshLayout swipeContainer;

    ArrayList<News> allnews;
    // public static int [] prgmImages={R.drawable.images,R.drawable.images1,R.drawable.images2,R.drawable.images3,R.drawable.images4,R.drawable.images5,R.drawable.images6,R.drawable.images7,R.drawable.images8};
    //public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};

    public Latest() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.trending, container, false);

        lv=(ListView) view.findViewById(R.id.listview);
        networkBanner=(TextView) view.findViewById(R.id.internet_status);
        loading = (SquareLoading) view.findViewById(R.id.progress_icon);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);

        if (isNetworkConnected())
        {
            showSuccessInConnection();
            loadData();
        }
        else
        {
            showErrorInConnection();
        }


        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                if (isNetworkConnected())
                {
                    showSuccessInConnection();
                    loadData();
                }
                else
                {
                    showErrorInConnection();
                }

            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //swipeContainer.setRefreshing(true);


        return view;
    }

    public void loadData()
    {
        swipeContainer.setRefreshing(true);
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest sr = new StringRequest(Request.Method.POST,links.RequestLink, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                swipeContainer.setRefreshing(false);
                allnews = new ArrayList<>();

                loading.setVisibility(View.GONE);

                try
                {
                    response = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                }
                catch (UnsupportedEncodingException e)
                {
                    e.printStackTrace();
                }

                try
                {
                    JSONArray job = new JSONArray(response);

                    for (int i = 0; i < job.length(); i++)
                    {
                        JSONObject c = job.getJSONObject(i);

                        String title = c.getString("title").trim();
                        title = new String(title.getBytes(), "UTF-8");

                        String paperName = c.getString("paper").trim();
                        paperName = new String(paperName.getBytes(), "UTF-8");

                        String catagory = c.getString("catagory").trim();
                        catagory = new String(catagory.getBytes(), "UTF-8");

                        String date = c.getString("date").trim();
                        date = new String(date.getBytes(), "UTF-8");

                        String time = c.getString("time").trim();
                        time = new String(time.getBytes(), "UTF-8");


                        String link = c.getString("link").trim();
                        link = new String(link.getBytes(), "UTF-8");

                        String views = c.getString("views").trim();
                        int nViews = Integer.parseInt(views);

                        String id = c.getString("newsID").trim();
                        int newsId = Integer.parseInt(id);


                        // Log.d("data",converted);
                        allnews.add(new News(paperName,catagory,title,link,date,time, nViews, newsId));
                    }


                }
                catch (JSONException e)
                {
                    Toast.makeText(getContext(),"cannot parse",Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                lv.setAdapter(new NewsAdapter(getActivity(), allnews));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                swipeContainer.setRefreshing(true);
                Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("tishpish",error.toString());
                //loadData();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("query","GET_DATA");
                return params;
            }


        };
        queue.add(sr);
    }

    protected boolean isNetworkConnected()
    {
        try
        {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            return (mNetworkInfo == null) ? false : true;

        }
        catch (NullPointerException e)
        {
            return false;

        }
    }


    public void showErrorInConnection()
    {
        if (networkBanner.getVisibility()== View.GONE)
            networkBanner.setVisibility(View.VISIBLE);

        networkBanner.setText(R.string.no_internet);
        networkBanner.setBackgroundColor(getResources().getColor(R.color.internet_no));
    }
    public void showSuccessInConnection()
    {
        if (networkBanner.getVisibility()== View.VISIBLE) {

            networkBanner.setText(R.string.yes_internet);
            networkBanner.setBackgroundColor(getResources().getColor(R.color.internet_yes));
            Handler handler = new Handler();
            Runnable delayrunnable = new Runnable() {
                @Override
                public void run() {
                    networkBanner.setVisibility(View.GONE);
                }
            };
            handler.postDelayed(delayrunnable, 3000);
        }

    }




}
