package headlines.top.tishpish.topheadlines.NewsData;

/**
 * Created by tishpish on 7/23/17.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
/**
 * Created by farhad on 5/8/16.
 */
public class AppSharedPreference
{
    private static AppSharedPreference mAppSharedPreference;

    private SharedPreferences mSharedPreferences;


    private Editor mEditor;
    private static Context mContext;

    /*
     * Implementing Singleton DP
     */
    private AppSharedPreference() {
        mSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        mEditor = mSharedPreferences.edit();
    }

    public static AppSharedPreference getInstance(Context context) {
        mContext = context;
        if (mAppSharedPreference == null)
            mAppSharedPreference = new AppSharedPreference();
        return mAppSharedPreference;
    }

    /*
        Storing the token after login
     */
    public String getSelectedCategory()
    {
        return mSharedPreferences.getString("category_name",
                "national");
    }

    public void putSelectedCategory(String token) {
        mEditor.putString("category_name", token);
        mEditor.commit();
    }

    public void putLoginInfo(String ID)
    {
        mEditor.putString("USER_ID", ID);
        mEditor.commit();
    }

    public String getLoginInfo()
    {
        return mSharedPreferences.getString("USER_ID",
                "");
    }
}